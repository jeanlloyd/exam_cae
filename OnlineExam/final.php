<?php include 'inc/header.php'; ?>
<?php
	Session::checkSession();
?>

<div class="main">
<h1>You are Done!</h1>

	<p>Congratulations ! You have just Completed the test.</p>
	<p>Final Sore :
		<?php
			if(isset($_SESSION['score'])){
				echo $_SESSION['score'];
				unset($_SESSION['score']);
			}
		?>
	</p>
	<a href="viewans.php" class="btn btn-info">View Correct Answer</a>
	<a href="starttest.php" class="btn btn-primary">Again Start Test!!!!</a>

  </div>
<?php include 'inc/footer.php'; ?>
