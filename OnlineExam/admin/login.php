<?php
    $filepath = realpath(dirname(__FILE__));
	include_once ($filepath.'/inc/loginheader.php');
	include_once ($filepath.'/../classes/Admin.php');
	$ad = new Admin();
?>
<?php
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		$adminData = $ad->getAdminData($_POST);
	}
?>


<div class="container">
  <h2> Admin Login Form</h2>

  <form action="" method="post">
    <div class="form-group">
      <label for="email">Email:</label>
      <input type="text" class="form-control" placeholder="Enter email" name="adminUser">
    </div>
    <div class="form-group">
      <label for="pwd">Password:</label>
      <input type="password" class="form-control" placeholder="Enter password" name="adminPass">
    </div>

    <button type="submit" name="login" class="btn btn-primary">Submit</button>
  </form>
  <?php
  if(isset($adminData)){
    echo $adminData;
  }
  ?>

<?php include 'inc/footer.php'; ?>
