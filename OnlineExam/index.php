<?php include 'inc/header.php'; ?>
<?php
	Session::checkLogin();
?>



<div class="container">
  <h2>Login Form</h2>
	<span class="empty" style ="display:none">Field Must Not be Empty !</span>
  <span class="error" style ="display:none">Email or Password not Matched !</span>
  <span class="disable" style ="display:none">User ID Disable !</span>
  <form action="" method="post">
    <div class="form-group">
      <label for="email">Email:</label>
      <input type="text" class="form-control" placeholder="Enter email" id="email">
    </div>
    <div class="form-group">
      <label for="pwd">Password:</label>
      <input type="password" class="form-control" placeholder="Enter password" id="password">
    </div>

    <button type="submit" id="loginsubmit" class="btn btn-primary">Submit</button>
  </form>

	<p>New User ? <a href="register.php">Signup</a> Free</p>

</div>
<?php include 'inc/footer.php'; ?>
